# Waze Map For Elementor
Contributors: codember, asadabir
Tags: elementor, waze map, google map, open street map
Donate link: https://codember.com/
Requires at least: 3.0.1
Tested up to: 5.5
Requires PHP: 5.4
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Easily intregate Waze map on Elementor as an alternative of Google Maps.

# Description

Waze Map for elementor allows you to easily intregate Waze map on an Elementor based site.


## How to use

* Install the plugin
* Go to Waze Map official website: [Waze Map](https://www.waze.com/livemap/)
* Search with your location
* Get the Latitude and Longitude form bottom corner: [Screenshot](https://prnt.sc/uxul6v)
* Place it to the Waze Map addon.
* Done


## Frequently Asked Questions

Qus: Does the plugin require any special settings/plugins?ß
Ans: You must install and activate Elementor before using this plugin.

# Screenshots
1. Waze Map Addons For Elementor
2. Waze Map Options

# Changelog

1.0: Initial Release